package com.comsatel.tarea.dao;

import java.util.List;


import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.comsatel.tarea.model.Tarea;

@Mapper
public interface ITareaDao {

	@Select("Select * from tarea")
	List<Tarea> findAll();
	
	@Insert("insert into tarea(descripcion,estado) values(#{descripcion},false)")
	void insert(Tarea tarea);
	
	@Update("update tarea set descripcion=#{descripcion},estado=#{estado} where idTarea = #{idTarea}")
	void update(Tarea tarea);
	
	@Delete("delete from tarea where idTarea=#{id}")
	void delete(@Param("id") int id);
	
	@Select("select * from tarea where idTarea=#{id}")
	Tarea findById(@Param("id") int id);
}
