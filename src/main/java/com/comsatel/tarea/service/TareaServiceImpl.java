package com.comsatel.tarea.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comsatel.tarea.dao.ITareaDao;
import com.comsatel.tarea.model.Tarea;

@Service
public class TareaServiceImpl implements TareaService{

	@Autowired
	private ITareaDao tareaDao;
	
	@Override
	public List<Tarea> listaTareas() {
		return tareaDao.findAll();
	}

	@Override
	public void registrar(Tarea tarea) {
		tareaDao.insert(tarea);
		
	}

	@Override
	public void actualizar(Tarea tarea) {
		tareaDao.update(tarea);
		
	}

	@Override
	public void eliminar(int id) {
		tareaDao.delete(id);
		
	}

	@Override
	public Tarea findById(int id) {
		return tareaDao.findById(id);
	}

}
