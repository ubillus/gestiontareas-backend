package com.comsatel.tarea.service;

import java.util.List;


import com.comsatel.tarea.model.Tarea;

public interface TareaService {

	public List<Tarea> listaTareas();
	void registrar(Tarea tarea);
	void actualizar(Tarea tarea);
	void eliminar(int id);
	Tarea findById(int id);
}
