package com.comsatel.tarea.model;

import lombok.Data;

@Data
public class Tarea {

	private int idTarea;
	private String descripcion;
	private boolean estado;
	
	public Tarea() {}

	public Tarea(int idTarea, String descripcion, boolean estado) {
		super();
		this.idTarea = idTarea;
		this.descripcion = descripcion;
		this.estado = estado;
	}
	
}
