package com.comsatel.tarea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comsatel.tarea.model.Tarea;
import com.comsatel.tarea.service.TareaService;

@RestController
@RequestMapping(value = "/api/tarea")
@CrossOrigin({"*"})
public class TareaController {

	@Autowired
	private TareaService tareaService;
	
	@GetMapping(value = "/listaTarea")
	public List<Tarea> ListadoTareas(){
		return tareaService.listaTareas();
	}
	
	@GetMapping(value = "/find/{id}")
	public Tarea find(@PathVariable("id")int id) {
		return tareaService.findById(id);
	}
	
	@PostMapping(value = "/save")
	public ResponseEntity<Tarea> save(@RequestBody Tarea tarea){
		if(tarea!=null) {
			tareaService.registrar(tarea);
		}else {
			return new ResponseEntity<Tarea>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Tarea>(tarea,HttpStatus.OK);
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<Tarea> update(@RequestBody Tarea tarea){
		if(tarea.getIdTarea()!=-1 || tarea.getIdTarea()!=0) {
			tareaService.actualizar(tarea);
		}else {
			return new ResponseEntity<Tarea>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Tarea>(tarea,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Tarea> delete(@PathVariable("id") int id){
		Tarea tarea = tareaService.findById(id);
		if(tarea !=null) {
			tareaService.eliminar(id);
		}else {
			return new ResponseEntity<Tarea>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Tarea>(tarea,HttpStatus.OK);
	}
	
	
	
	
}
